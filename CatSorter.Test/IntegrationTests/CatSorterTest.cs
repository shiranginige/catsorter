using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CatSorter.Test
{
	public class CatSorterTest
	{
		private readonly CatSorter _catSorter;
		public CatSorterTest()
		{
			_catSorter = new CatSorter();
		}

		[Fact]
		public async Task fetchandfilter_should_return_valid_results()
		{
			var results = await _catSorter.FetchDataAndFilter();
			Assert.True(results.Count() > 0);

		}

	}
}
