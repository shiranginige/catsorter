using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CatSorter.Test
{
	public class CatPeopleFilterTest
	{
		private readonly IList<Person> _people;
		private readonly CatPeopleFilter _catPeopleFilter;
		public CatPeopleFilterTest()
		{
			_people = new List<Person>()
			{
				new Person{
					Gender =  "Male" , Name = "James" ,
					Pets = new List<Pet>{
					 new Pet { Name="Timmy" , Type="Cat" }
					,new Pet { Type= "Dog" ,Name= "Fido" }
					}
				},
				new Person{
					Gender =  "Female" , Name = "Natalie" ,
					Pets = new List<Pet>{
					 new Pet { Name="Garfield" , Type="Cat" },
					 new Pet { Name="Junior Garfield" , Type="Cat" },
					 new Pet { Name="Tabby" , Type="Cat" }
					,new Pet { Type= "Dog" ,Name= "Dido" }
					}
				},
				new Person{
					Gender =  "Female" , Name = "Loren" ,
					Pets = null
				}
			};

			_catPeopleFilter = new CatPeopleFilter();

		}

		[Fact]
		public void filter_with_a_valid_dataset_should_return_the_correct_count()
		{
			var result = _catPeopleFilter.Filter(_people);
			Assert.Equal(result.Count(), 2);
		}


		[Fact]
		public void filter_with_a_valid_dataset_should_have_cat_names_sorted()
		{
			var result = _catPeopleFilter.Filter(_people);
			var nataliesCats = result.ElementAt(1);
			var catNames = nataliesCats.Cats;
			Assert.Equal(catNames.Count(), 3);
			Assert.True(string.Compare(catNames[1], catNames[0]) > 0);
			Assert.True(string.Compare(catNames[2], catNames[1]) > 0);
		}

		[Fact]
		public void filter_with_an_invalid_input_should_not_throw_an_exception()
		{
			 Assert.Throws<ArgumentException>( () => _catPeopleFilter.Filter(null));
		}
	}
}
