using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CatSorter.Test
{
	public class DataFetcherTest
	{

		private readonly DataFetcher<Person> _dataFetcher;
		public DataFetcherTest()
		{
			_dataFetcher = new DataFetcher<Person>();
		}

		[Fact]
		public async Task fetch_with_a_valid_url_should_return_valid_results()
		{
			var results = await _dataFetcher.FetchList("http://agl-developer-test.azurewebsites.net/people.json");
			Assert.True(results.Count() > 0);
		}

		[Fact]
		public async Task fetch_with_a_wrong_url_should_throw_a_business_exception()
		{
			await Assert.ThrowsAsync<BusinessException>(async () => await _dataFetcher.FetchList(""));
		}
	}
}
