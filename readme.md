# Tech Stack 

* .Net Core 1.1 

* XUnit

Note : Please make sure [.net core 1.1 runtime](https://www.microsoft.com/net/download/core#/runtime) is installed before proceeding to below steps


# Running Tests

* ` $ cd CatSorter.Test`
* ` $ dotnet restore `
* ` $ dotnet test`


# Running the App

* `$ cd CatSorter`
* `$ dotnet restore `
* `$ dotnet run`
