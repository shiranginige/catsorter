﻿using System.Collections.Generic;
using System.Linq;

namespace CatSorter
{
	public static class ExtensionMethods
	{
		public static bool IsEmpty<T>(this IEnumerable<T> list)
		{
			return list.IsNull() || list.Count() == 0;
		}

		public static bool IsNotEmpty<T>(this IEnumerable<T> list)
		{
			return !list.IsEmpty();
		}

		public static bool IsNull(this object item)
		{
			return item == null;
		}
		public static bool IsNotNull(this object item)
		{
			return !item.IsNull();
		}


	}

}
