﻿using System.Collections.Generic;
using System.Linq;

namespace CatSorter
{
	public class Person
	{

		public string Name { get; set; }
		public string Gender { get; set; }
		public IEnumerable<Pet> Pets { get; set; }

		private string[] GetCatsSorted()
		{
			if (Pets.IsNotEmpty())
			{
				return Pets
					.Where(a => a.Type == "Cat")
					.OrderBy(a => a.Name)
					.Select(a => a.Name)
					.ToArray();
			}
			return new string []{ };
		}

		public string[] Cats
		{
			get
			{
				return GetCatsSorted();
			}
		}
	}

}
