﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CatSorter
{
	public class CatSorter
	{
		public async Task<IEnumerable<Person>> FetchDataAndFilter()
		{
			var fetcher = new DataFetcher<Person>();
			var data = await fetcher.FetchList("http://agl-developer-test.azurewebsites.net/people.json");

			var catPeopleFilter = new CatPeopleFilter();
			return catPeopleFilter.Filter(data);
		}
	}

}
