﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CatSorter
{
	public class DataFetcher<T>
	{
		public async Task<IEnumerable<T>> FetchList(string peopleUrl)
		{
			using (var httpClient = new HttpClient())
			{
				try
				{
					var jsonResult = await httpClient.GetStringAsync(peopleUrl);
					var result = JsonConvert.DeserializeObject<IEnumerable<T>>(jsonResult);
					return result;
				}
				catch
				{
					throw new BusinessException("An error occured calling the specified url or it didn't return valid data");
				}
			}
		}
	}
}
