﻿using System;

namespace CatSorter
{
	public class BusinessException : Exception
	{
		public BusinessException(string message) : base(message)
		{

		}
	}
}
