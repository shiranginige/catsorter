﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CatSorter
{
	public class CatPeopleFilter
	{
		public IEnumerable<Person> Filter(IEnumerable<Person> people)
		{
			if (people.IsNull())
				throw new ArgumentException("invalid people collection");
			return people.Where(a => a.Cats.Length > 0);

		}
	}

}
