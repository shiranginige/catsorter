﻿using System;

namespace CatSorter
{
	public static class Entry
	{
		static void Main(string[] args)
		{
			var _catSorter = new CatSorter();
			var people = _catSorter.FetchDataAndFilter().Result;

			foreach (var person in people)
			{
				Console.WriteLine(person.Gender);
				foreach (var cat in person.Cats)
				{
					Console.WriteLine($" * {cat}");
				}
			}

			Console.ReadLine();
		}
	}

}
